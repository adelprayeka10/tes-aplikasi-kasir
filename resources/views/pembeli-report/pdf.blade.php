<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>

    <style>
    .text-center{
      text-align: center;
    }
    table, td, th {
      border: 0.7px solid;
      text-align: left;
    }

    table {
      border-collapse: collapse;
      width: 100%;
    }

    th, td {
      padding: 7px;
    }
    .header{
      font-size: 20px;
      font-style: oblique;
      font-weight: bold;
    }
    </style>
  </head>
  <body>
    <label class="header"> <b>GAMAPAY</b> <br> Universitas Gadjah Mada</label>
    <hr>
    <div>
      <label style="padding-right: 58px;"> Pembeli : {{$pembeli->name}} </label>
    </div>
    <div>
      <label style="padding-right: 58px;"> Email : {{$pembeli->email}}</label>
    </div>
    <div>
      <label style="padding-right: 0px;"> No.Handphone : {{$pembeli->no_telepon}}</label>
    </div>
    <div>
      @if (\Carbon\Carbon::parse($request->from) == \Carbon\Carbon::parse($request->until)->addDay(-1))
      <label style="padding-right: 36px;"> Tanggal : {{\Carbon\Carbon::parse($request->from)->formatLocalized('%A, %d %B %Y')}}</label>
      @else
      <label style="padding-right: 36px;">Tanggal : {{\Carbon\Carbon::parse($request->from)->formatLocalized('%A, %d %B %Y')}} - {{\Carbon\Carbon::parse($request->until)->formatLocalized('%A, %d %B %Y')}}</label>
      @endif
    </div>
    <div>
      <label style="padding-right: 8px;"> Total Belanja : {{$outcomeTotal}} </label>
    </div>
    <div>
      <label style="padding-right: 32px;"> Total Produk  : {{$itemTotal}} </label>
    </div>
    <br>
    <table id="datatable1" class="table table-striped table-bordered table-hover">
      <thead>
        <tr>
          <th>No</th>
          <th>Id Transaksi</th>
          <th>Tanggal</th>
          <th>Produk</th>
          <th>Toko</th>
          <th>Harga</th>
          <th>Jumlah</th>
          <th>Total</th>
          <th>Jenis Pembayaran</th>
          <th>Status</th>
        </tr>
      </thead>
      @if (\Carbon\Carbon::parse($request->from)->addDay(1) == \Carbon\Carbon::parse($request->until))
        <tbody>
          @foreach($list as $value => $detail_transaksi)
            <tr>
                <td align="center">{{$value+1}}</td>
                <td>{{$detail_transaksi->transaksi_konven_id}}</td>
                <td>{{\Carbon\Carbon::parse($detail_transaksi->updated_at)->format('H:i:s')}}</td>
                <td>{{$detail_transaksi->nama}}</td>
                <td>{{$detail_transaksi->nama_toko}}</td>
                <td>{{$detail_transaksi->harga}}</td>
                <td>{{$detail_transaksi->kuantitas}}</td>
                @php
                  $jumlah = $detail_transaksi->jumlah;
                  $harga = $detail_transaksi->harga;
                  $income = $jumlah * $harga;
                @endphp
                <td>{{$income}}</td>
                <td>{{$detail_transaksi->jenis_pembayaran}}</td>
                <td>{{$detail_transaksi->status}}</td>
            </tr>
          @endforeach
        </tbody>
      @else
        <tbody>
          @foreach($list as $value => $detail_transaksi)
            <tr>
                <td align="center">{{$value+1}}</td>
                <td>{{$detail_transaksi->transaksi_konven_id}}</td>
                <td>{{\Carbon\Carbon::parse($detail_transaksi->updated_at)->formatLocalized('%A, %d %B %Y')}}<br>{{\Carbon\Carbon::parse($detail_transaksi->updated_at)->format('H:i:s')}}</td>
                <td>{{$detail_transaksi->nama}}</td>
                <td>{{$detail_transaksi->nama_toko}}</td>
                <td>{{$detail_transaksi->harga}}</td>
                <td>{{$detail_transaksi->jumlah}}</td>
                @php
                  $jumlah = $detail_transaksi->jumlah;
                  $harga = $detail_transaksi->harga;
                  $income = $jumlah * $harga;
                @endphp
                <td>{{$income}}</td>
                <td>{{$detail_transaksi->jenis_pembayaran}}</td>
                <td>{{$detail_transaksi->status}}</td>
            </tr>
          @endforeach
        </tbody>
      @endif


    </table>
    <label> <b>Aplikasi Kasir Berbasis Web Gamapay.</b></label>
  </body>
</html>
