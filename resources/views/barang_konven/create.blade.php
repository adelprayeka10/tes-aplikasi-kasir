@extends('master')

@section('css')
  <link href="{{asset('assets/vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet">
@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Dashboard</a> /
  <a href="#">Data Master</a> /
  <a href="{{url('/master/barang_konven')}}">Produk</a> /
  <a href="{{url('/master/barang_konven/add')}}">Tambah</a>
@stop

@section('title')
  <h3>Data Master</h3>
@stop

@section('title')
    <div class="sl-page-title">
        <h5>Tambah Produk</h5>
    </div><!-- sl-page-title -->
@stop

@section('content')

<div id="app">  
          @if(Session::has('success'))
            <div class="alert alert-success">
              {{ Session::get('success') }}
            </div>
          @elseif(Session::has('error'))
            <div class="alert alert-error">
              {{ Session::get('error') }}
            </div>
          @endif
        </div>

 <div class="exportexcel">
            <form class="form-horizontal pull-right" enctype="multipart/form-data">
              <a href="{{url('master/barang_konven/downloadTemplateExcel')}}" class="btn btn-warning">Download Template Import Produk<i class="mg-r-10 fa fa-download"></i></a> 
            </form>

            <form  action="{{ url('master/barang_konven/import') }}" class="form-horizontal pull-left" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              
              @if ($errors->has('import'))
              <span class="invalid-feedback" role="alert" style="color: red">
                  <strong>{{ $errors->first('import') }}</strong>
              </span>
              @endif
              <input type="file" name="import" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"/>
              
              <button class="btn btn-success pull-left">Import File</button>
            </form>
        </div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Tambah Produk</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

              {!! Form::open(array('url'=>'master/barang_konven/store', 'method'=>'POST', 'class'=>'form-horizontal form-label-left', 'novalidate', 'enctype'=>'multipart/form-data', 'files'=>'true'))!!}
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Kategori <span class="required" style="color: red">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control select2" name="kategori" class="form-control col-md-7 col-xs-12">
                      <option value="">Pilih Kategori</option>
                      @foreach($kategori as $value => $key)
                          <option value="{{$key->id}}" {{collect(old('kategori'))->contains($key->id) ? 'selected':''}}>{{$key->kategori}}</option>
                      @endforeach
                  </select>
                  @if ($errors->has('kategori'))
                  <span class="invalid-feedback" role="alert" style="color: red">
                      <strong>{{ $errors->first('kategori') }}</strong>
                  </span>
                  @endif
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Nama Produk <span class="required" style="color: red">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input class="form-control" placeholder="nama produk" name="nama" value="{{ old('nama') }}" type="text" class="form-control col-md-7 col-xs-12">
                  @if ($errors->has('nama'))
                  <span class="invalid-feedback" role="alert" style="color: red">
                      <strong>{{ $errors->first('nama') }}</strong>
                  </span>
                  @endif
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Deskripsi
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <textarea id="textarea" placeholder="tulis deskripsi" name="deskripsi"  type="text" class="form-control col-md-7 col-xs-12">{{ old('deskripsi') }}</textarea>
                  @if ($errors->has('deskripsi'))
                  <span class="invalid-feedback" role="alert" style="color: red">
                      <strong>{{ $errors->first('deskripsi') }}</strong>
                  </span>
                  @endif
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Harga <span class="required" style="color: red">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input class="form-control" placeholder="harga" name="price" value="{{ old('harga') }}" type="text" class="form-control col-md-7 col-xs-12">
                   @if ($errors->has('price'))
                  <span class="invalid-feedback" role="alert" style="color: red">
                      <strong>{{ $errors->first('price') }}</strong>
                  </span>
                  @endif
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">Status <span class="required" style="color: red">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control select2" name="status" class="form-control col-md-7 col-xs-12">
                      <option value="">Pilih Status</option>
                          <option value="Tersedia">Tersedia</option>
                          <option value="Habis">Habis</option>
                  </select>
                  @if ($errors->has('stock'))
                  <span class="invalid-feedback" role="alert" style="color: red">
                      <strong>{{ $errors->first('stock') }}</strong>
                  </span>
                  @endif
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="discount">Diskon
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input class="form-control" placeholder="Diskon" name="discount" value="{{ old('diskon') }}" type="text" class="form-control col-md-7 col-xs-12">
                  @if ($errors->has('discount'))
                  <span class="invalid-feedback" role="alert" style="color: red">
                      <strong>{{ $errors->first('discount') }}</strong>
                  </span>
                  @endif
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Gambar
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="file" class="form-control" name="foto" placeholder="" value="{{old('foto')}}">
                </div>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                  <a class="btn btn-primary" onclick="location.href='{{url('master/barang_konven')}}'">Batal</a>
                  <button id="send" type="submit" class="btn btn-success">Simpan</button>
                </div>
              </div>
              {!!Form::close()!!}
          </div>
        </div>
      </div>
    </div>
<!-- /page content -->

        <!-- row -->
@endsection

@section('javascript')
    <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendors/select2/dist/js/select2.min.js')}}"></script>
    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
                //scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
            $('.select2').select2();
        });
    </script>

@endsection
