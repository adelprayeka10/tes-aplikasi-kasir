@section('js')
<script type="text/javascript">
  $(document).ready(function() {
    $('#table').DataTable({
      "iDisplayLength": 50
    });

} );
</script>

@stop
@extends('layout_pos')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{ $produk }}</h3>

              <p>Jumlah Produk</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ $transaksi }}</h3>

              <p>Jumlah Transaksi</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>3</h3>

              <p>Jumlah Pembeli</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{ $hari_ini }}</h3>

              <p>Transaksi Hari Ini</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-lg-6 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3>Rp {{number_format($saldo_hari_ini,0,".",".")}},-</h3>

              <p>Jumlah Pemasukan Hari Ini (Keseluruhan)</p>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
          </div>
        </div>

        <div class="col-lg-6 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-purple">
            <div class="inner">
              <h3>Rp {{number_format($gamapay_hari_ini,0,".",".")}},-</h3>

              <p>Jumlah Pemasukan Gamapay Hari Ini</p>
            </div>
            <div class="icon">
              <i class="fa fa-dollar"></i>
            </div>
          </div>
        </div>

      </div>

      <div class="row">

        <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Produk Terlaris</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Barang</th>
                  <th>Terjual</th>
                </tr>
                </thead>
                <tbody>
                @foreach($popu as $value => $transaksi)
                <tr>
                  <td align="center">{{$value+1}}</td>
                  <td>{{$transaksi->nama}}</td>
                  <td>{{$transaksi->jmlhbarang}}</td>
                </tr>
                @endforeach
                </tbody>
              </table>

            </div>
             
            <!-- ./box-body -->
            <div class="box-footer" align="center">
              
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->

        <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Tidak Tersedia</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Barang</th>
                  <th>Keterangan</th>
                </tr>
                </thead>
                <tbody>
                @foreach($barang as $value => $transaksi)
                <tr>
                  <td align="center">{{$value+1}}</td>
                  <td>{{$transaksi->nama}}</td>
                  <td><a class="btn btn-danger btn-xs rounded-circle mg-r-5 mg-b-10" title="Habis">
                        <div><i class="fa fa-circle-o"></i> Habis</div>
                      </a>
                      <a href="{{url('barang_konven/edit_pos/'.$transaksi->id.'')}}" class="btn btn-warning btn-xs rounded-circle mg-r-5 mg-b-10" title="Tambah Stok">
                        <div><i class="fa fa-plus"></i> Ubah Status</div>
                      </a>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>

            </div>
             
            <!-- ./box-body -->
            <div class="box-footer" align="center">
              
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->


      </div>
      <!-- /.row -->
      <section class="content" id="chart1">
          {!! $chart1 !!}
      </section>
    </section>
        
    
@endsection
