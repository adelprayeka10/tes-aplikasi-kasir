<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Kantin Kejujuran UGM">
  <meta name="author" content="KOMSI">

  <!-- App Favicon -->
  <link rel="shortcut icon" href="{{asset('img/logo_pln.jpg')}}">


  <title>{{env('SITE_TITLE','Kantin Kejujuran UGM')}}</title>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="{{asset('lib/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
  <link href="{{asset('lib/Ionicons/css/ionicons.css')}}" rel="stylesheet">

  <!-- Starlight CSS -->
  <link rel="stylesheet" href="{{asset('css/starlight.css')}}">
</head>

<body>

<div class="d-flex align-items-center justify-content-center bg-sl-primary ht-100v">

        <div class="login-wrapper pd-100 pd-xs-100 bg-white">

            <div class="tx-center mg-b-10">
                <img src="{{asset('img/logo2.png')}}" style="height: 50px;" alt="" class="text-center"/>
            </div>
            <div class="signin-logo tx-center tx-32 tx-bold tx-inverse mg-b-30">KANTIN <span class="tx-info tx-bold">KEJUJURAN</span>
            </div>

            <form role="form" class="form-layout" method="POST" action="{{ route('login') }}">
              @csrf

            <div class="form-group row">
                <label for="email" class="col-sm-5 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                <div class="col-md-7">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="password" class="col-md-5 col-form-label text-md-right">{{ __('Password') }}</label>

                <div class="col-md-7">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-6 offset-md-5">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-8 offset-md-5">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Login') }}
                    </button>

                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                </div>
            </div>
    <!-- login-wrapper -->
</div>
<!-- d-flex -->

<script src="{{asset('lib/jquery/jquery.js')}}"></script>
<script src="{{asset('lib/popper.js/popper.js')}}"></script>
<script src="{{asset('lib/bootstrap/bootstrap.js')}}"></script>

<script src="{{asset('js/konami.js')}}"></script>
<script src="{{asset('js/code.js')}}"></script>

</body>
</html>
