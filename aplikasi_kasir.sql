-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 08, 2019 at 04:31 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aplikasi_kasir`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang_konven`
--

CREATE TABLE `barang_konven` (
  `id` int(10) UNSIGNED NOT NULL,
  `kategori_id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(75) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `harga` int(10) UNSIGNED NOT NULL,
  `status` enum('Tersedia','Habis') COLLATE utf8mb4_unicode_ci NOT NULL,
  `diskon` int(10) UNSIGNED DEFAULT '0',
  `foto` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'product.png',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `barang_konven`
--

INSERT INTO `barang_konven` (`id`, `kategori_id`, `nama`, `deskripsi`, `harga`, `status`, `diskon`, `foto`, `created_at`, `updated_at`) VALUES
(42, 1, 'Nasi Goreng Sapi', 'Nasi Goreng Dengan Daging Sapi Pilihan', 25000, 'Habis', 0, 'product.png', '2019-12-08 05:30:52', '2019-12-08 07:07:18'),
(43, 1, 'Mie Goreng Rendang', 'Mie GOreng Dengan Rendang', 18000, 'Tersedia', 0, 'product.png', '2019-12-08 05:31:37', '2019-12-08 05:31:37'),
(44, 2, 'Es Teh Manis', 'Es Teh Segar', 3000, 'Tersedia', 0, 'product.png', '2019-12-08 05:32:06', '2019-12-08 05:32:06'),
(45, 2, 'Es Susu Sirup', 'Es ditambah susu dtambah sirup', 5000, 'Tersedia', 0, 'product.png', '2019-12-08 05:32:52', '2019-12-08 05:32:52'),
(46, 1, 'Nasi Padang Enak', 'Nasi Padang Dengan Sambal Matcha', 150000, 'Tersedia', 0, 'product.png', '2019-12-08 07:22:30', '2019-12-08 07:22:30');

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksi_konven`
--

CREATE TABLE `detail_transaksi_konven` (
  `id` int(10) UNSIGNED NOT NULL,
  `barang_konven_id` int(10) UNSIGNED NOT NULL,
  `transaksi_konven_id` int(10) UNSIGNED NOT NULL,
  `harga` int(10) UNSIGNED NOT NULL,
  `diskon` int(10) NOT NULL,
  `jumlah` int(10) UNSIGNED NOT NULL,
  `total` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `detail_transaksi_konven`
--

INSERT INTO `detail_transaksi_konven` (`id`, `barang_konven_id`, `transaksi_konven_id`, `harga`, `diskon`, `jumlah`, `total`, `created_at`, `updated_at`) VALUES
(1, 43, 19, 18000, 0, 1, 18000, '2019-12-08 06:34:38', '2019-12-08 06:34:38'),
(2, 44, 19, 3000, 0, 1, 3000, '2019-12-08 06:34:38', '2019-12-08 06:34:38'),
(3, 42, 20, 25000, 0, 1, 25000, '2019-12-08 06:55:46', '2019-12-08 06:55:46'),
(4, 46, 21, 150000, 0, 1, 150000, '2019-12-08 07:22:59', '2019-12-08 07:22:59'),
(5, 43, 22, 18000, 0, 1, 18000, '2019-12-08 15:09:22', '2019-12-08 15:09:22'),
(6, 44, 22, 3000, 0, 3, 9000, '2019-12-08 15:09:22', '2019-12-08 15:09:22');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(10) UNSIGNED NOT NULL,
  `kategori` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `kategori`, `deskripsi`, `created_at`, `updated_at`) VALUES
(1, 'Makanan', 'Semua jenis makanan enak', '2018-11-27 00:27:03', '2019-03-12 21:40:42'),
(2, 'Minuman', '', '2018-11-27 00:27:38', '2018-11-27 00:27:38');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_12_08_153013_create_barang_konven_table', 0),
(2, '2019_12_08_153013_create_detail_transaksi_konven_table', 0),
(3, '2019_12_08_153013_create_kategori_table', 0),
(4, '2019_12_08_153013_create_oauth_access_tokens_table', 0),
(5, '2019_12_08_153013_create_oauth_auth_codes_table', 0),
(6, '2019_12_08_153013_create_oauth_clients_table', 0),
(7, '2019_12_08_153013_create_oauth_personal_access_clients_table', 0),
(8, '2019_12_08_153013_create_oauth_refresh_tokens_table', 0),
(9, '2019_12_08_153013_create_transaksi_konven_table', 0),
(10, '2019_12_08_153013_create_users_table', 0),
(11, '2019_12_08_153019_add_foreign_keys_to_barang_konven_table', 0),
(12, '2019_12_08_153019_add_foreign_keys_to_detail_transaksi_konven_table', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('100dc778cf346a4d4bfcd3ed0d19ca53518a5d811f7b17870df70cc79167f6875a8a65ecf2d98976', 10, 1, 'MyApp', '[]', 0, '2019-04-12 19:22:53', '2019-04-12 19:22:53', '2020-04-13 02:22:53'),
('12994614e2e449648db194f217e375da64c34bb020fe0c189ae39e45a1b926eea1dc331778b819ae', 3, 1, 'MyApp', '[]', 0, '2019-04-14 19:53:33', '2019-04-14 19:53:33', '2020-04-15 02:53:33'),
('1d7dbaa067e24876c81a360c86981109963a68adc9e4e5ad27db47bbf26173d713a88dcf0a1c52f8', 3, 1, 'MyApp', '[]', 0, '2019-04-14 19:50:55', '2019-04-14 19:50:55', '2020-04-15 02:50:55'),
('482ca97aa50b1651db1d82585c7e536e9ffffae6e0f0c9cc6d8e0b2bcf0943e50423fb3907c8a689', 3, 1, 'MyApp', '[]', 0, '2019-04-14 20:00:32', '2019-04-14 20:00:32', '2020-04-15 03:00:32'),
('4bb21b54f662a6b3db700cf4f03139fd5b66a5b5445a16468f4ed3d5cff94385422f7ae52f2ed645', 3, 1, 'MyApp', '[]', 0, '2019-04-14 19:59:03', '2019-04-14 19:59:03', '2020-04-15 02:59:03'),
('51e732f5cc6363b7d302c6f53acbabc0a0c1038881259535db9cdbb257b1bd3c236c14211a962d32', 12, 1, 'MyApp', '[]', 0, '2019-03-19 18:56:10', '2019-03-19 18:56:10', '2020-03-20 01:56:10'),
('619f62637ee85574b6b614d4532d81ef6163c83f31ad325ac415ad59f2eafbfaf5e17a39c92bee85', 3, 1, 'MyApp', '[]', 0, '2019-04-14 19:50:04', '2019-04-14 19:50:04', '2020-04-15 02:50:04'),
('72d28771be26f1e0ef0569fa68887d8a57df1986055d803e101a5722cb6f94af46d0ebd75fd32bc6', 10, 1, 'MyApp', '[]', 0, '2019-03-17 23:48:55', '2019-03-17 23:48:55', '2020-03-18 06:48:55'),
('8695c3ce85c04cf8daeda22b85ba330acdc0c62b2ccadfe3945542a5729d28fe9b90add0f28e6b9e', 3, 1, 'MyApp', '[]', 0, '2019-04-14 19:56:33', '2019-04-14 19:56:33', '2020-04-15 02:56:33'),
('8709dbbcaf5b36a82d737bc9db73c622360b07cabc342ea8658d78b2fa3c8ce3c25207cb5864a93c', 13, 1, 'MyApp', '[]', 0, '2019-03-24 07:02:13', '2019-03-24 07:02:13', '2020-03-24 14:02:13'),
('8f9103fd7e31d03b3190246268be95b8a8badad7419e602208911af0ed961a45ec51b559647b0009', 12, 1, 'MyApp', '[]', 0, '2019-03-24 02:37:17', '2019-03-24 02:37:17', '2020-03-24 09:37:17'),
('9abe2186bc52447c029296e4954725dea293a0a1cf14741378374561c7de6bcded54fe6441c497cb', 4, 1, 'MyApp', '[]', 0, '2019-03-03 05:50:01', '2019-03-03 05:50:01', '2020-03-03 12:50:01'),
('a835be9cd66ce5cf77581b101df57b5fcb511c35140f3462674530497a572af057f0baf95bccc240', 3, 1, 'MyApp', '[]', 0, '2019-03-13 20:40:42', '2019-03-13 20:40:42', '2020-03-14 03:40:42'),
('b0d4d373f83c97698e5e761ccff585060c622360d579083526db4f1fc6a9ed27022ff4a53ca708ab', 10, 1, 'MyApp', '[]', 0, '2019-03-13 21:42:12', '2019-03-13 21:42:12', '2020-03-14 04:42:12'),
('b1ce6d993516fa8a0f09f48f6571cdc0f85249e1ff08f363cbf2f2d345ffce4becd548ede0d4ed36', 3, 1, 'MyApp', '[]', 0, '2019-03-13 21:48:14', '2019-03-13 21:48:14', '2020-03-14 04:48:14'),
('bf704c204b4e646b0153fc729a46338ea8b31cd74653eeb0bee1685e0a479aac01c92a74bed44098', 3, 1, 'MyApp', '[]', 0, '2019-03-17 22:54:24', '2019-03-17 22:54:24', '2020-03-18 05:54:24'),
('bf9251f13a72f3760cb5225d4869b655b168a9538d538bb3676fe500c50a0af605b4782b3c722c85', 10, 1, 'MyApp', '[]', 0, '2019-03-13 20:53:48', '2019-03-13 20:53:48', '2020-03-14 03:53:48'),
('debe91b09af533e132a770ae9c9d92324cbdeac78454035ffc0e61c4321daf8e097a2bdde155a903', 12, 1, 'MyApp', '[]', 0, '2019-03-21 02:07:42', '2019-03-21 02:07:42', '2020-03-21 09:07:42'),
('e1126c777a18cfe9399c1cecda3172fea1b91b8ce862c516f3338363c928ba9ecc6bd5271cb415f7', 13, 1, 'MyApp', '[]', 0, '2019-03-13 20:41:38', '2019-03-13 20:41:38', '2020-03-14 03:41:38'),
('e678c6512b59f2a2fa36783908255deee48507047f0590fa3032be938037b3c4ee0ccba5f1fac4f7', 13, 1, 'MyApp', '[]', 0, '2019-03-24 07:00:59', '2019-03-24 07:00:59', '2020-03-24 14:00:59'),
('ec366166ee50c12edfef5ebcb70a22513eba8221e803b0acf1b1ca970a7f61735ca2b05e19db3033', 12, 1, 'MyApp', '[]', 0, '2019-03-03 05:53:05', '2019-03-03 05:53:05', '2020-03-03 12:53:05'),
('f029a845c2fe944c8a050748e537112d5b68ae2bc011a29b2128a30df6317ab452d4be9cacee0981', 3, 1, 'MyApp', '[]', 0, '2019-04-14 19:49:42', '2019-04-14 19:49:42', '2020-04-15 02:49:42');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'zt7SCPU1ArbcZip2nw51XxXzbe0R7F9J0oDboMYn', 'http://localhost', 1, 0, 0, '2019-03-03 01:18:57', '2019-03-03 01:18:57'),
(2, NULL, 'Laravel Password Grant Client', 'mJp9KPflJLZd5tSmzxWapol8SX7QZGx6p5PvBtj6', 'http://localhost', 0, 1, 0, '2019-03-03 01:18:58', '2019-03-03 01:18:58');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-03-03 01:18:58', '2019-03-03 01:18:58');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_konven`
--

CREATE TABLE `transaksi_konven` (
  `id` int(10) UNSIGNED NOT NULL,
  `no_meja` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diskon` int(10) DEFAULT '0',
  `total` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaksi_konven`
--

INSERT INTO `transaksi_konven` (`id`, `no_meja`, `status`, `diskon`, `total`, `created_at`, `updated_at`) VALUES
(19, '1', 'Selesai', 0, 21000, '2019-12-08 06:34:38', '2019-12-08 08:00:53'),
(20, '69', 'Selesai', 0, 25000, '2019-12-08 06:55:46', '2019-12-08 07:58:46'),
(21, '15', 'Aktif', 0, 150000, '2019-12-08 07:22:59', '2019-12-08 07:22:59'),
(22, '28', 'Aktif', NULL, 27000, '2019-12-08 15:09:22', '2019-12-08 15:09:22');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `level` enum('Admin','Pelayan','Kasir') COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telepon` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `foto` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'avatar.png',
  `saldo` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `status_id` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `level`, `name`, `email`, `email_verified_at`, `password`, `no_telepon`, `tanggal_lahir`, `foto`, `saldo`, `status_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Admin', 'admin@gmail.com', '2018-11-25 20:06:10', '$2y$10$J98mh1FlCScDufzKK61u7eo2j7REi5EkX56SNaQXyQ8EYncCY3Rey', '823456781', '2018-11-01', 'avatar.png', 0, 2, 'NBIrwZeodBsr48ljDcKvualdnIuLfGrfeVCP4jCPpHqJOYrEwf7F2yt3Hur2', NULL, '2019-04-14 10:05:44'),
(2, 'Kasir', 'Kasir', 'kasir@gmail.com', '2019-01-13 19:12:12', '$2y$10$J98mh1FlCScDufzKK61u7eo2j7REi5EkX56SNaQXyQ8EYncCY3Rey', '812345668', '2017-11-15', 'avatar.png', 0, 2, 'Awt9TKCZAhm0XsTxtfGztWexVZZaAYcJ22xXAqWQp0eCbtSpxFHCVq0nLuLl', '2019-01-14 21:08:00', '2019-12-08 08:01:14'),
(3, 'Pelayan', 'Pelayan', 'pelayan@gmail.com', '2019-01-16 23:00:00', '$2y$10$J98mh1FlCScDufzKK61u7eo2j7REi5EkX56SNaQXyQ8EYncCY3Rey', '898765', '2018-10-10', 'Layndo.jpg', 100000, 2, 'b6IZ22fnHlTlwqrUnVRKKt8LKEnHQkQCFUSkq2x3NDUA2ykeuoLwjoHHMGFR', '2019-01-16 17:00:00', '2019-12-08 08:09:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang_konven`
--
ALTER TABLE `barang_konven`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang_konven_kategori_id_foreign` (`kategori_id`);

--
-- Indexes for table `detail_transaksi_konven`
--
ALTER TABLE `detail_transaksi_konven`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detail_transaksi_konven_barang_konven_id_foreign` (`barang_konven_id`),
  ADD KEY `detail_transaksi_konven_transaksi_konven_id_foreign` (`transaksi_konven_id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `transaksi_konven`
--
ALTER TABLE `transaksi_konven`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang_konven`
--
ALTER TABLE `barang_konven`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `detail_transaksi_konven`
--
ALTER TABLE `detail_transaksi_konven`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `transaksi_konven`
--
ALTER TABLE `transaksi_konven`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `barang_konven`
--
ALTER TABLE `barang_konven`
  ADD CONSTRAINT `barang_konven_kategori_id_foreign` FOREIGN KEY (`kategori_id`) REFERENCES `kategori` (`id`);

--
-- Constraints for table `detail_transaksi_konven`
--
ALTER TABLE `detail_transaksi_konven`
  ADD CONSTRAINT `detail_transaksi_konven_barang_konven_id_foreign` FOREIGN KEY (`barang_konven_id`) REFERENCES `barang_konven` (`id`),
  ADD CONSTRAINT `detail_transaksi_konven_transaksi_konven_id_foreign` FOREIGN KEY (`transaksi_konven_id`) REFERENCES `transaksi_konven` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
