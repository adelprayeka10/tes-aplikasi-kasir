<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBarangKonvenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('barang_konven', function(Blueprint $table)
		{
			$table->foreign('kategori_id')->references('id')->on('kategori')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('barang_konven', function(Blueprint $table)
		{
			$table->dropForeign('barang_konven_kategori_id_foreign');
		});
	}

}
