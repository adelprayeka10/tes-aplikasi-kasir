<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBarangKonvenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('barang_konven', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('kategori_id')->unsigned()->index('barang_konven_kategori_id_foreign');
			$table->string('nama', 45);
			$table->string('deskripsi', 75)->nullable();
			$table->integer('harga')->unsigned();
			$table->enum('status', array('Tersedia','Habis'));
			$table->integer('diskon')->unsigned()->nullable()->default(0);
			$table->string('foto', 100)->nullable()->default('product.png');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('barang_konven');
	}

}
