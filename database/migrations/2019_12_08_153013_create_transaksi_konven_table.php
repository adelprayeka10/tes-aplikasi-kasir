<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransaksiKonvenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transaksi_konven', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('no_meja', 50);
			$table->string('status', 30);
			$table->integer('diskon')->nullable()->default(0);
			$table->integer('total')->unsigned();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transaksi_konven');
	}

}
