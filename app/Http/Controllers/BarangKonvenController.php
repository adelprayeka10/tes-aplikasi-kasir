<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use File;
use App\BarangKonven;
use App\Kategori;
use App\PenjualKonven;
use App\User;
use DB;
use Auth;
use Excel;

class BarangKonvenController extends Controller
{
  public function __construct()
  {
      // $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = DB::table('barang_konven')
                ->join('kategori', 'barang_konven.kategori_id', '=', 'kategori.id')
                ->select('barang_konven.*', 'kategori.kategori')
                ->get()
                ->toArray();
        return view('barang_konven.list', compact('list'));
    }

    public function index2()
    {
        
        $list = DB::table('barang_konven')
                ->join('kategori', 'barang_konven.kategori_id', '=', 'kategori.id')
                ->select('barang_konven.*', 'kategori.kategori')
                ->get()
                ->toArray();
        return view('barang_konven.list_pos', compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::all();
        return view('barang_konven.create', compact('kategori'));
    }

    public function create2()
    {
        $kategori = Kategori::all();
        $penjual_konven = User::all();
        return view('barang_konven.create_pos', compact('kategori','penjual_konven'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      date_default_timezone_set('Asia/Jakarta');

      $this->validate(request(),
          [
            'kategori' => 'required',
            'nama' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)|max:45',
            'price' => 'required|max:999999999',
            'status' => 'required',
          ],
          [
            'kategori.required' => 'Kategori belum dipilih!',
            'nama.required' => 'Nama harus diisi!',
            'nama.regex' => 'Nama tidak boleh memakai angka!',
            'nama.max' => 'Nama terlalu panjang!',
            'price.required' => 'Harga harus diisi!',
            'price.max' => 'Nominal harga terlalu banyak!',
            'status.required' => 'Status harus diisi!',
          ]
        );

        if(!empty($request->foto)){
           $file = $request->file('foto');
           $extension = strtolower($file->getClientOriginalExtension());
           $filename = $request->nama . '.' . $extension;
           Storage::put('images/' . $filename, File::get($file));
           $file_server = Storage::get('images/' . $filename);
           $img = Image::make($file_server)->resize(141, 141);
           $img->save(base_path('public/images/' . $filename));
         }else{
           $filename='product.png';
         }

        if(!empty($request->discount)){
           $diskon=$request->discount;
         }else{
           $diskon='0';
         }

         if(!empty($request->deskripsi)){
           $deskripsi=$request->deskripsi;
         }else{
           $deskripsi='-';
         }

        BarangKonven::create([
          'kategori_id'=>request('kategori'),
          'nama'=>request('nama'),
          'deskripsi'=>$deskripsi,
          'harga'=>request('price'),
          'status'=>request('status'),
          'diskon'=>$diskon,
          'foto'=>$filename
        ]);
      return redirect('master/barang_konven')->with('success','Barang berhasil ditambahkan.');
    }

    public function store2(Request $request)
    {

        date_default_timezone_set('Asia/Jakarta');

        $this->validate(request(),
          [
            'kategori' => 'required',
            'nama' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)|max:45',
            'harga' => 'required|max:999999999',
            'stok' => 'required',
          ],
          [
            'kategori.required' => 'Kategori belum dipilih!',
            'nama.required' => 'Nama harus diisi!',
            'nama.regex' => 'Nama tidak boleh memakai angka!',
            'nama.max' => 'Nama terlalu panjang!',
            'harga.required' => 'Harga harus diisi!',
            'harga.max' => 'Nominal harga terlalu besar!',
            'stok.required' => 'Status harus diisi!',
          ]
        );

        if(!empty($request->foto)){
           $file = $request->file('foto');
           $extension = strtolower($file->getClientOriginalExtension());
           $filename = $request->nama . '.' . $extension;
           Storage::put('images/' . $filename, File::get($file));
           $file_server = Storage::get('images/' . $filename);
           $img = Image::make($file_server)->resize(141, 141);
           $img->save(base_path('public/images/' . $filename));
         }else{
           $filename='product.png';
         }

         if(!empty($request->diskon)){
           $diskon=$request->diskon;
         }else{
           $diskon='0';
         }

         if(!empty($request->deskripsi)){
           $deskripsi=$request->deskripsi;
         }else{
           $deskripsi='-';
         }

        BarangKonven::create([
          'kategori_id'=>request('kategori'),
          'nama'=>request('nama'),
          'deskripsi'=>$deskripsi,
          'harga'=>request('harga'),
          'status'=>request('stok'),
          'diskon'=>$diskon,
          'foto'=>$filename
        ]);
      return redirect('barang_konven/list_pos')->with('success','Barang berhasil ditambah.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = BarangKonven::findOrFail($id);
      $status = BarangKonven::where('id',$id)->select('status')->get();
      $kategori = Kategori::all()->sortBy('kategori');
      return view('barang_konven.edit',compact('data','kategori','status'));
    }

    public function edit2($id)
    {
      $data = BarangKonven::findOrFail($id);
      $kategori = Kategori::all()->sortBy('kategori');
      return view('barang_konven.edit_pos',compact('data','kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      date_default_timezone_set('Asia/Jakarta');
      $gambar = DB::table('barang_konven')->where('id', '=', $id)->pluck('foto')->first();
      // dd($gambar);

      $this->validate(request(),
          [
            'kategori' => 'required',
            'nama' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)|max:45',
            'harga' => 'required|max:999999999',
            'stok' => 'required',
          ],
          [
            'kategori.required' => 'Kategori belum dipilih!',
            'nama.required' => 'Nama harus diisi!',
            'nama.regex' => 'Nama tidak boleh memakai angka!',
            'nama.max' => 'Nama terlalu panjang!',
            'harga.required' => 'Harga harus diisi!',
            'harga.required' => 'Nominal harga terlalu besar!',
            'stok.required' => 'Status harus diisi!',
          ]
        );

      if(!empty($request->foto)){
           $file = $request->file('foto');
           $extension = strtolower($file->getClientOriginalExtension());
           $filename = $request->nama . '.' . $extension;
           Storage::put('images/' . $filename, File::get($file));
           $file_server = Storage::get('images/' . $filename);
           $img = Image::make($file_server)->resize(141, 141);
           $img->save(base_path('public/images/' . $filename));
         }else{
           $filename=$gambar;
         }

      if(!empty($request->diskon)){
           $diskon=$request->diskon;
         }else{
           $diskon='0';
         }

         if(!empty($request->deskripsi)){
           $deskripsi=$request->deskripsi;
         }else{
           $deskripsi='-';
         }


      $data = BarangKonven::find($id);
      $data->kategori_id=$request->get('kategori');
      $data->nama=$request->get('nama');
      $data->deskripsi=$deskripsi;
      $data->harga=$request->get('harga');
      $data->status=$request->get('stok');
      $data->diskon=$diskon;
      $data->foto=$filename;

      $data->save();
      return redirect('master/barang_konven')->with('success', 'Data barang berhasil diubah.');
    }

    public function update2(Request $request, $id)
    {
      date_default_timezone_set('Asia/Jakarta');
      $gambar = DB::table('barang_konven')->where('id', '=', $id)->pluck('foto')->first();
      // dd($gambar);

      $this->validate(request(),
          [
            'kategori' => 'required',
            'nama' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)|max:45',
            'harga' => 'required|max:999999999',
            'stok' => 'required',
          ],
          [
            'kategori.required' => 'Kategori belum dipilih!',
            'nama.required' => 'Nama harus diisi!',
            'nama.regex' => 'Nama tidak boleh memakai angka!',
            'nama.max' => 'Nama terlalu panjang!',
            'harga.required' => 'Harga harus diisi!',
            'harga.required' => 'Nominal harga terlalu besar!',
            'stok.required' => 'Status harus diisi!',
          ]
        );

      if(!empty($request->foto)){
           $file = $request->file('foto');
           $extension = strtolower($file->getClientOriginalExtension());
           $filename = $request->nama . '.' . $extension;
           Storage::put('images/' . $filename, File::get($file));
           $file_server = Storage::get('images/' . $filename);
           $img = Image::make($file_server)->resize(141, 141);
           $img->save(base_path('public/images/' . $filename));
         }else{
           $filename=$gambar;
         }

         if(!empty($request->diskon)){
           $diskon=$request->diskon;
         }else{
           $diskon='0';
         }

         if(!empty($request->deskripsi)){
           $deskripsi=$request->deskripsi;
         }else{
           $deskripsi='-';
         }

      $data = BarangKonven::find($id);
      $data->kategori_id=$request->kategori;
      $data->nama=$request->nama;
      $data->diskon=$diskon;
      $data->deskripsi=$deskripsi;
      $data->status=$request->stok;
      $data->harga=$request->harga;
      $data->foto=$filename;

      $data->save();
      return redirect('barang_konven/list_pos')->with('success', 'Data barang berhasil diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $cek2 = DB::table('detail_transaksi_konven')
              ->join('barang_konven', 'detail_transaksi_konven.barang_konven_id', '=', 'barang_konven.id')
              ->where('barang_konven.id',$id)
              ->get();

        // remove id from KATEGORI
        if($cek2->isNotEmpty()){
              return redirect()->back()->with('error', 'Produk yang dipilih masih memiliki data transaksi.');
        }else{

        $data = BarangKonven::find($id)->delete();
 
        return redirect()->back()->with('success', 'Data berhasil di hapus.');
        }
    }

    public function delete($id)
    {
        $cek2 = DB::table('detail_transaksi_konven')
              ->join('barang_konven', 'detail_transaksi_konven.barang_konven_id', '=', 'barang_konven.id')
              ->where('barang_konven.id',$id)
              ->get();

        // remove id from KATEGORI
        if($cek2->isNotEmpty()){
              return redirect()->back()->with('error', 'Produk yang dipilih masih memiliki data transaksi.');
        }else{

        $data = BarangKonven::find($id)->delete();
 
        return redirect()->back()->with('success', 'Data berhasil di hapus.');
        }
    }

    public function exportFileMaster(Request $request)
    {
            $data = DB::table('barang_konven')
                ->join('penjual_konven', 'barang_konven.penjual_konven_id', '=', 'penjual_konven.id')
                ->join('kategori', 'barang_konven.kategori_id', '=', 'kategori.id')
                ->select('barang_konven.id','barang_konven.nama','kategori.kategori','barang_konven.deskripsi','barang_konven.harga','barang_konven.status','barang_konven.diskon')
                ->get()
                ->toArray();

            $data= json_decode( json_encode($data), true);
            Excel::create('DataBarang', function($excel) use($data){
            $excel->sheet('Data Barang', function ($sheet) use ($data) {
                $sheet->fromArray($data);
                });
            })->download("xlsx");
    }

    public function exportFile(Request $request)
    {
            $user = Auth::user()->penjual_konven()->pluck('id')->first();

            $data = DB::table('barang_konven')
                ->join('penjual_konven', 'barang_konven.penjual_konven_id', '=', 'penjual_konven.id')
                ->join('kategori', 'barang_konven.kategori_id', '=', 'kategori.id')
                ->select('barang_konven.id','barang_konven.nama','kategori.kategori','barang_konven.deskripsi','barang_konven.harga','barang_konven.status','barang_konven.diskon')
                ->where('barang_konven.penjual_konven_id','=',$user)
                ->get()
                ->toArray();

            $data= json_decode( json_encode($data), true);
            Excel::create('DataBarang', function($excel) use($data){
            $excel->sheet('Data Barang', function ($sheet) use ($data) {
                $sheet->fromArray($data);
                });
            })->download("xlsx");
    }

    public function downloadTemplateExcelAdmin(Request $request)
    {
        $data = BarangKonven::where('id','0')->get()->toArray();
        return Excel::create('Template Import Produk Admin', function($excel) use ($data) {
            $excel->sheet('import_produk', function($sheet) use ($data)
            {
                $sheet->cell('A1', function($cell) {$cell->setValue('nama_toko');   });
                $sheet->cell('B1', function($cell) {$cell->setValue('nama_kategori');   });
                $sheet->cell('C1', function($cell) {$cell->setValue('nama_produk');   });
                $sheet->cell('D1', function($cell) {$cell->setValue('deskripsi');   });
                $sheet->cell('E1', function($cell) {$cell->setValue('harga_rupiah');   });
                $sheet->cell('F1', function($cell) {$cell->setValue('status');   });
                $sheet->cell('G1', function($cell) {$cell->setValue('diskon_rupiah');   });

            });
        })->download("xlsx");
    }

    public function downloadTemplateExcelPenjual(Request $request)
    {
        $data = BarangKonven::where('id','0')->get()->toArray();
        return Excel::create('Template Import Produk Admin', function($excel) use ($data) {
            $excel->sheet('import_produk', function($sheet) use ($data)
            {
                $sheet->cell('A1', function($cell) {$cell->setValue('nama_kategori');   });
                $sheet->cell('B1', function($cell) {$cell->setValue('nama_produk');   });
                $sheet->cell('C1', function($cell) {$cell->setValue('deskripsi');   });
                $sheet->cell('D1', function($cell) {$cell->setValue('harga_rupiah');   });
                $sheet->cell('E1', function($cell) {$cell->setValue('status');   });
                $sheet->cell('F1', function($cell) {$cell->setValue('diskon_rupiah');   });

            });
        })->download("xlsx");
    }

    public function import(Request $request)
    {
      $this->validate($request,
      [
        'import' => 'required',
      ],
      [
        'import.required' => 'File belum dimasukan!',
      ]);

        

        if ($request->hasFile('import')) {
            $path = $request->file('import')->getRealPath();

            $data = Excel::load($path, function($reader){})->get();
            $a = collect($data);            

            $k=$a->first()->keys()->toArray();
            $diff = count(array_diff($k, array('nama_toko','nama_kategori','nama_produk','deskripsi','harga_rupiah','status','diskon_rupiah')));

            if($diff>0){
                return back()->with('error', 'Format Salah!');
            }

            if (!empty($a) && $a->count()) {
                foreach ($a as $key => $value) {

            $cek1 = DB::table('barang_konven')->where('nama', $value->nama_produk.' '.$value->nama_toko)->get();
            $kategori = DB::table('kategori')->where('kategori', $value->nama_kategori)->pluck('id')->first();
            $penjual = DB::table('penjual_konven')->where('nama_toko', $value->nama_toko)->pluck('id')->first();
            
            if($cek1->isNotEmpty()){
                return back()->with('error', 'Produk'.' '.  $value->nama_produk.' '.$value->nama_toko .' '. 'yang dimasukan sudah terdaftar. Silahkan cek ulang juga data yang berada dibawahnya kemudian lakukan import ulang!');
            }else{

                $insert[] = [
                            'penjual_konven_id' => $penjual,
                            'kategori_id' => $kategori, 
                            'nama' => $value->nama_produk.' '.$value->nama_toko,
                            'deskripsi' => $value->deskripsi,
                            'harga' => $value->harga_rupiah,
                            'status' => $value->status,
                            'diskon' => $value->diskon_rupiah,
                            ];

                        BarangKonven::create($insert[$key]);

                    }
                  }
                    return redirect('master/barang_konven')->with('success', 'Data berhasil di import.');
            };            
        }
    }

    public function importFile(Request $request)
    {
      $this->validate($request,
      [
        'import' => 'required',
      ],
      [
        'import.required' => 'File belum dimasukan!',
      ]);
      $user = Auth::user()->penjual_konven()->pluck('id')->first();        

        if ($request->hasFile('import')) {
            $path = $request->file('import')->getRealPath();

            $data = Excel::load($path, function($reader){})->get();
            $a = collect($data);            

            $k=$a->first()->keys()->toArray();
            $diff = count(array_diff($k, array('nama_kategori','nama_produk','deskripsi','harga_rupiah','status','diskon_rupiah')));

            if($diff>0){
                return back()->with('error', 'Format Salah!');
            }

            if (!empty($a) && $a->count()) {
                foreach ($a as $key => $value) {

            $cek1 = DB::table('barang_konven')->where('nama', $value->nama_produk.' '.$value->nama_toko)->get();
            $kategori = DB::table('kategori')->where('kategori', $value->nama_kategori)->pluck('id')->first();
            $penjual = DB::table('penjual_konven')->where('id', $user)->pluck('nama_toko')->first();

            if($cek1->isNotEmpty()){
                return back()->with('error', 'Produk'.' '.  $value->nama_produk.' '.$penjual .' '. 'yang dimasukan sudah terdaftar. Silahkan cek ulang juga data yang berada dibawahnya kemudian lakukan import ulang!');
            }else{

                $insert[] = [
                            'penjual_konven_id' => $user,
                            'kategori_id' => $kategori, 
                            'nama' => $value->nama_produk.' '.$penjual,
                            'deskripsi' => $value->deskripsi,
                            'harga' => $value->harga_rupiah,
                            'status' => $value->status,
                            'diskon' => $value->diskon_rupiah,
                            ];

                        BarangKonven::create($insert[$key]);

                      }

                    }
                    return redirect('barang_konven/list_pos')->with('success', 'Data berhasil di import.');
            };            
        }
    }

    // START OF API Controller

    // Api lihat semua barang dan toko
    public function api_index()
    {
        $barang = BarangKonven::leftJoin('penjual_konven', 'barang_konven.penjual_konven_id', '=', 'penjual_konven.id')
                          ->select('barang_konven.*','penjual_konven.nama_toko')
                          ->get();
        return response()->json([
          'status'=>'success',
          'result'=>$barang
        ]);
    }

    // API lihat barang pennjual berdasarkan id
    public function api_index_penjual()
    {

        $user = Auth::user()->penjual_konven()->pluck('id')->first();  
        $produk=BarangKonven::where('penjual_konven_id','=',$user)->get()->sortBy('name');
        return response()->json([
          'status'=>'success',
          'result'=>$produk
        ]);
    }

}
