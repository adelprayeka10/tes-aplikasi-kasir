<?php

namespace App\Http\Controllers;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use File;
use Illuminate\Http\Request;
use App\User;
Use Alert;
Use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;


class ProfilController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index($id)
  {
      $user = User::find($id);
      return view('profil.index', compact('user'));
  }
  public function edit($id)
  {
    $data = User::find($id);
    return view('profil.edit',compact('data'));
  }
  public function editPassword($id)
  {
    $data = User::find($id);
    return view('profil.edit-password',compact('data'));
  }
  public function update(Request $request, $id)
  {
    date_default_timezone_set('Asia/Jakarta');
    $data= User::find($id);
    $data->email=$request->email;
    $data->name=$request->name;
    $data->no_telepon=$request->no_hp;
    $data->tanggal_lahir=$request->tanggal_lahir = Carbon::parse($request->tanggal_lahir);
    // $data->password=bcrypt($request->password);
    $this->validate($request,
      [
        'name' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)',
        'no_hp' => 'required',
        'tanggal_lahir' => 'required',
        // 'saldo' => 'required',
      ],
      [
        'name.required' => 'Nama harus diisi!',
        'name.regex' => 'Nama tidak bisa berupa angka!',
        'no_hp.required' => 'Nomor telepon harus diisi!',
        'tanggal_lahir.required' => 'Tanggal lahir harus diisi!',
        // 'no_telepon.unique' => 'Phone number has already been taken!',
        // 'saldo.required' => 'Saldo can not be empty!'
      ]);
    $data->save();
    return redirect('profile/'.$id.'')->with('success', 'Data berhasil diubah!');
  }
  public function updatePassword(Request $request, $id)
  {
    date_default_timezone_set('Asia/Jakarta');
    $data= User::find($id);
    if(Hash::check($request->password_current,$data->password)){
      $data->password = Hash::make($request->password);
      $this->validate($request,
        [
          'password_current' => 'required',
          'password' => 'required|string|min:8|confirmed',
          'password_confirmation' => 'required',
          // 'password_confirmation' => 'required|string|min:8|confirmed',
        ],
        [
          // 'name.required' => 'Name can not be empty!',
        ]);
      $data->save();
      return redirect('profile/'.$id.'')->with('success', 'Data berhasil diubah!');
    }else{
        // Alert::warning('Warning!','Something went wrong!');
        // return redirect()->back();
        return redirect()->back()->with('error', 'Password does not match!');
    }
  }
  public function editAvatar($id)
  {

      $data = User::find($id);
      return view('profil.edit-ava', compact('data'));
  }
  public function updateAvatar(Request $request, $id)
  {
    date_default_timezone_set('Asia/Jakarta');
    $data= User::where('id',$id)->first();

    $this->validate($request,
    [
      'foto' => 'required',
    ],
    [
      'foto.required' => 'Foto belum dipilih!',
    ]);

    $file = $request->file('foto');
    $extension = strtolower($file->getClientOriginalExtension());
    $filename = $data->name . '.' . $extension;
    Storage::put('images/' . $filename, File::get($file));
    $file_server = Storage::get('images/' . $filename);
    $img = Image::make($file_server)->resize(141, 141);
    $img->save(base_path('public/images/' . $filename));

    $data->foto=$filename;
    $data->save();
    return redirect('profile/'.$data->id.'')->with('success', 'Foto berhasil diubah!');
  }
}
