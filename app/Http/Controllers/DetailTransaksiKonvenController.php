<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DetailTransaksiKonven;
use App\TransaksiKonven;
use App\BarangKonven;
use App\Pembeli;
use App\User;
use App\PenjualKonven;
use App\JenisPembayaran;
use Cart;
use DB;
use Auth;
use PDF;
use Response;

class DetailTransaksiKonvenController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        date_default_timezone_set('Asia/Jakarta');
        
        return view('transaksi_koven.index',compact('produk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $barang = BarangKonven::select('status')->where('id', $request->id_produk)->get();

        $transaksi['status']='Aktif';
        $transaksi['diskon']=$request->diskon;
        $transaksi['total']=$request->total;
        $transaksi['no_meja']=$request->no_meja;
        $transaksi['created_at']=date('Y-m-d H:i:s');
        $transaksi['updated_at']=date('Y-m-d H:i:s');
        $getId=TransaksiKonven::insertGetId($transaksi);

        // $a = $request->id_produk;

        foreach ($request->id_produk as $key => $value) {

            $transaksi_konven_id=$getId;
            $harga=$request->harga[$key];
            $potongan=$request->potongan[$key];
            $jumlah=$request->jumlah[$key];
            $subtotal=$request->subtotal[$key];
            DetailTransaksiKonven::Create(['transaksi_konven_id'=>$transaksi_konven_id,'barang_konven_id'=>$value,'harga'=>$harga, 'diskon'=>$potongan,'jumlah'=>$jumlah,'total'=>$subtotal]);
            // BarangKonven::where('id', $request->id_produk[$key])->decrement('stok', $jumlah);
            }   
            
        // return redirect('transaksi_konven')->with('success','Transaksi berhasil.');
        return redirect('invoice/'.$getId)->with('success','Transaksi berhasil disimpan.');
    }

    //print nota
    public function getPdf($id)
    {
        date_default_timezone_set('Asia/Jakarta');       

        $data = DetailTransaksiKonven::with('transaksiKonven','barang_konven')->where('transaksi_konven_id',$id)->get()->toArray();

        return view('transaksi_koven.nota',compact('value','data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('transaksi_koven.list');
    }

    function fetch(Request $request)
    {
     if($request->get('query'))
     {
      $query = $request->get('query');
      $data = DB::table('barang_konven')
        ->where('nama', 'LIKE', "%{$query}%")
        ->where('status','Tersedia')
        ->get();

        if($data->isEmpty()){
          $output = '<ul class="dropdown-menu col-md-12" style="display:block; position:relative"><li><a href="#">'.'Tidak ditemukan/Stok habis'.'</a></li></ul>';
          echo $output;
        }

        else{
          $output = '<ul class="dropdown-menu col-md-12" style="display:block; position:relative">';
          foreach($data as $row)
          {
           $output .= '
           <li><a href="#">'.$row->nama.'</a></li>
           ';
          }
          $output .= '</ul>';
          echo $output;

        }
     }
    }

    public function getData($id_produk)
    {
        $tampil = BarangKonven::find($id_produk);
        return response()->json($tampil);
    }


    public function getDataProduk(Request $request)
    {
        $tampil = BarangKonven::where('nama',$request->nama)->where('status','Tersedia')->first();
        return response()->json($tampil);
    }

    public function getDataPembeli($id_pembeli)
    {
      $tampil = DB::table('users')
                ->join('pembeli', 'users.id', '=', 'pembeli.user_id')
                ->where('pembeli.id',$id_pembeli)
                ->select('users.saldo', 'pembeli.member','pembeli.id','pembeli.nama')
                ->get()->first();

        return response()->json($tampil);
    }

    public function getNamaPembeli(Request $request)
    {

      $tampil = DB::table('users')
                ->join('pembeli', 'users.id', '=', 'pembeli.user_id')
                ->where('pembeli.nama',$request->nama_pembeli)
                ->select('users.saldo', 'pembeli.member','pembeli.id','pembeli.nama')
                ->get()->first();
        // $tampil = Pembeli::where('nama',$request->nama_pembeli)->first();
        return response()->json($tampil);
    }

    public function autoComplete(Request $request) {
        $query = $request->get('term','');
        $user = Auth::user()->penjual_konven()->pluck('id')->first();  

        $products=BarangKonven::where('nama','LIKE','%'.$query.'%')->where('penjual_konven_id','=',$user)->get();
        
        $data=array();
        foreach ($products as $product) {
            $data[]=array('value'=>$product->nama,'id'=>$product->id);
        }
        if(count($data))
            return $data;
        else
            return ['value'=>'Hasil tidak ditemukan','id'=>''];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
