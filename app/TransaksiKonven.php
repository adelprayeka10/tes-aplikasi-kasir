<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiKonven extends Model
{
    protected $table = 'transaksi_konven';
  	protected $fillable = ['id','no_meja','status','total','diskon'];
  	public $timestamps = true;

  public function detailTransaksiKonven()
  {
    return $this->hasMany('App\DetailTransaksiKonven', 'transaksi_konven_id', 'id');
  }
}
